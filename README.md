# Mapa Conceptual "VON-NEUMANN vs HARDVARD"
```plantuml
@startmindmap
*[#LightCoral] **Von-Neumann vs Hardvard**  
	*[#B89B8D] **Von-Newmann**
		*_ Creo
			*[#BB8FCE] Arquitectura del computador Actual en 1945 \n tomando, Bloques funcionales que conforman una computadora
				*_ tales como:
					*[#D2B4DE] -CPU: Unidad Central de Proceso, se encarga de operaciones basicás. \n -Memoria Principal: Almacena datos e instrucciones. \n -Buses: Perimte la comunicacion entre bloques del sistema. \n -Perifericos: Entrada y Salida de datos.
				*_ propiedades
					*[#F5B041] 1. Unico espacio de memoria. \n 2. El contenido es accesible por posición. \n 3. La ejecucion de las instrucciones es secuancial.
	*[#33B9AB] **Hardvard**
		*[#B2BABB] Modelo creado por Howar H. AIken en 1944.
			*_ formado por:
				*[#ABEBC6] -Memoria de instrucciones. \n -Procesador. \n -Memoria de Datos. \n -Buses Independiantes.
					*[#D4AC0D] Por sus componenetes tenía Acceso de forma \n independiente y simultanea a la memoria de datos \n y a la de instrucciones.
					*_ de igual forma
						*[#E74C3C] Los buses contenian informacion en la misma ubicación \n de las diferentes memorias.
@endmindmap
```
# Mapa Conceptual "Supercomputadoras en México"
```plantuml
@startmindmap
*[#0BCFE7] **Supercomputadoras en México**
	*[#58D68D] **IBM-650**
		*_ fue:
			*[#28B463] Primer computadora en México y en Latinoameria,\n instalada en la UNAM, el 18 de junio de 1958. 
		*[#1D8348] Funcionaba mediante tarjetas perforadas,\n realizaba mil operaciones aritmeticas por segundo y \n contaba con 2K de memoria.
	*[#E59866] **CRAY-432**
		*_ fue:
			*[#CA6F1E] Primer Supercomputadora de Latinoamerica,\n instalada en la UNAM en el año de 1991.
		*[#AF601A] Se considero que equivalia a 2 mil \n computadoras de oficina y \n permanecio en uso durante 10 años.
	*[#E74C3C] **KanBalam**
		*_ fue:
			*[#CB4335] Una supercomputadora que residia en la UNAM, \n se consideraba que procesaba hasta \n 7 millones de operaciones por segundo.
		*[#B03A2E] Su uso fue exclusivo para realizar \n investigaciones sobre Astronomia,\n Quimca-Cuantica,Nanotecnologia, entre otros.
	*[#2980B9] **Miztli**
		*_ es:
			*[#2471A3] Una supercomputadora, que igual reisde en \n la UNAM, se enfoca en predicciones \n del clima, estudio efectivo de sistemas \n diseño de nuevos materiales y farmacos.
		*[#21618C] Cuenta con 8344 procesadores,\n es capaz de realizar 228 billones de \n operaciones de punto flotante en 1 segundo.
	*[#95A5A6] **Xiuhcoatl**
		*_ es:
			*[#839192] Cluster Hibrido de Supercomputo,creada en el 2012 \n es cosiderada la segunda supercomputadora \n más veloz de Latinoamerica.
		*[#B3B6B7] Conecta a tres instituciones:\n UNAM, UAM y el Cinvestav del IPN \n puede realizar hasta \n 252 billones de operaciones de punto flotante.
	*[#884EA0] **Supercomputadora de BUAP**
		*_ es:
			*[#76448A] Una supercomputdaora ubicada en la \n Benemérita Universidad Autónoma de Puebla \n con ella se realizan investigaciones de \n fisica, quimica, astronomia y bioinformatica.
		*[#633974] En un segundo puede realizar hasta \n 2 mil millones de operaciones, su uidad \n de almacenamiento es equivalente a 5 mil computadoras portatiles.
@endmindmap
```